#include<vector>
#include<cassert>
#include<string>

struct Player
{
	std::wstring imie;
	int punkty;
};

struct Punkt
{
	int x;
	int y;
	Punkt(int xx = 0, int yy = 0) : x(xx), y(yy) { }
	bool operator==(const Punkt & p) const { return p.x == x && p.y == y; }
};
enum Kierunek { N, S, W, E };

enum State { BEGIN_GAME, GAME, END_GAME, RECORDS, BEAT_RECORD, MENU };

class Snake
{
private:
	int dlugosc;
	std::vector<Punkt> punkty;
	Kierunek zwrot;
	Punkt granica_gorna, granica_dolna;
	int skok;
	bool rosnie;
	Punkt jedzenie;
	Punkt losuj_jedzenie();
public:
	Snake() { }
	Snake(int dl, Punkt beg, Kierunek zw, Punkt gr_g, Punkt gr_d, int sk) : dlugosc(dl), 
		zwrot(zw), granica_gorna(gr_g), granica_dolna(gr_d), skok(sk), rosnie(false)
	{
		int i;
		for(i = 0; i < dl; i++)
			punkty.push_back(Punkt(beg.x + i*skok, beg.y));
		jedzenie = losuj_jedzenie();
	}
	bool ruch();
	bool czy_zderzenie();
	void przyrost() { rosnie = true; }
	bool czy_przyrost() { return rosnie; }
	Kierunek kierunek() const { return zwrot; }
	bool zmien_kierunek(Kierunek k);
	std::vector<Punkt>::const_iterator beg() const { return punkty.begin(); } 
	std::vector<Punkt>::const_iterator end() const { return punkty.end(); }
	Punkt zarcie() const { return jedzenie; }
};