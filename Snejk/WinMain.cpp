#include<Windows.h>
#include<string>
#include<sstream>
#include<vector>
#include<fstream>
#include<cstdlib>
#include<ctime>
#include<cctype>
#include"Snake.h"

std::wstring WindowClassName = L"dupa001gej";
 
const int skok = 10;
const char * plik = "data/wyniki.ble";
const wchar_t * menu_bmp = L"data/Menu.bmp";
const wchar_t * strzalka_bmp = L"data/Strzalka.bmp";

void tworz_domyslny_plik()
{

	std::wofstream out(plik);
	for(int i = 0; i < 10; i++)
	{
		std::wostringstream out_int;
		out << L"ZIOM\t";
		out_int << 10 - i*1;
		out << out_int.str().c_str() << L"\t";
		
	}
	out.close();
}

void zapisz_do_pliku(std::wstring imie_lista, int wynik, int pozycja_lista)
{
	Player gracze[10];
	std::wifstream in(plik);
	std::wofstream out;
	for(int i = 0; i < 10; i++)
	{
		if( i != pozycja_lista )
			in >> gracze[i].imie >> gracze[i].punkty;
		else
		{
			if( imie_lista.length() == 0)
				imie_lista = L"-";
			gracze[i].imie = imie_lista;
			gracze[i].punkty = wynik;
		}
	}
	out.open(plik);
	for(int i = 0; i < 10; i++)
	{
		std::wostringstream out_int;
		out << gracze[i].imie.c_str() << L"\t";
		out_int << gracze[i].punkty;
		out << out_int.str().c_str() << "\t";
	}
}

void odswiez_strzalke(HWND hOkno)
{
	RECT rec;
	rec.left = 0;
	rec.top = 220;
	rec.right = 100;
	rec.bottom = 470;
	InvalidateRect(hOkno,&rec,true);
}

void odswiez_okno(HWND hOkno)
{
	RECT rec;
	GetClientRect(hOkno,&rec);
	InvalidateRect(hOkno,&rec,true);
}

void odswiez_szybkosc(HWND hOkno)
{
	RECT rec;
	rec.left = 395;
	rec.top = 350;
	rec.right = 425;
	rec.bottom = 375;
	InvalidateRect(hOkno,&rec,true);
}

LRESULT CALLBACK WindowEventProc(HWND hOkno, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	static Snake waz;
	static bool poruszony = false;
	static int wynik;
	static std::wstring wynik_char;
	static State Stan = MENU;
	static int pozycja_lista;
	static std::wstring imie_lista;
	static int wybor = 0;
	static int szybkosc = 0;

	switch(Stan)
	{
	case MENU:
		switch(Msg)
		{
		case WM_PAINT:
			{
				std::wostringstream int_char;
				int_char << szybkosc;
				HBITMAP menu = (HBITMAP) LoadImage(NULL, menu_bmp, IMAGE_BITMAP,
					0, 0, LR_LOADFROMFILE);
				HBITMAP strzalka = (HBITMAP) LoadImage(NULL, strzalka_bmp, IMAGE_BITMAP, 
					0, 0, LR_LOADFROMFILE);
				BITMAP Rozmiar_menu, Rozmiar_strzalka;
				GetObject(menu, sizeof(BITMAP),&Rozmiar_menu);
				GetObject(strzalka, sizeof(BITMAP),&Rozmiar_strzalka);
				HDC HDCokno, HDCpamiec;
				PAINTSTRUCT ps;
				HDCokno = BeginPaint(hOkno,&ps);
				HDCpamiec = CreateCompatibleDC(HDCokno);
				DeleteObject( SelectObject(HDCpamiec, (HBITMAP) menu) );
				BitBlt(HDCokno,0 ,0 ,Rozmiar_menu.bmWidth ,Rozmiar_menu.bmHeight ,HDCpamiec,
					0,0,SRCCOPY);
				DeleteObject( SelectObject(HDCpamiec, (HBITMAP) strzalka) );
				TransparentBlt(HDCokno,0 ,220 + 50*wybor ,Rozmiar_strzalka.bmWidth ,Rozmiar_strzalka.bmHeight ,
					HDCpamiec,0,0, Rozmiar_strzalka.bmWidth, Rozmiar_strzalka.bmHeight,
					GetPixel(HDCpamiec,0,0) );
				DeleteObject( SelectObject( HDCokno,CreateFontW(30,25,0,0,800,0,0,0,0,
					0,0,0,0,L"Arial")) );
				SetBkMode(HDCokno,TRANSPARENT);
				TextOut(HDCokno,395,350,int_char.str().c_str(), int_char.str().length());
				EndPaint(hOkno,&ps);
			}
		case WM_KEYDOWN:
			switch(wParam)
			{
			case VK_DOWN:
				wybor = (wybor + 1) % 4;
				odswiez_strzalke(hOkno);
				return 0;
			case VK_UP:
				wybor--;
				if( wybor == -1)
					wybor = 3;
				odswiez_strzalke(hOkno);
				return 0;
			case VK_RETURN:
				switch(wybor)
				{
				case 2 :
					return 0;
				case 0:
					Stan = BEGIN_GAME;
					break;
				case 1:
					Stan = RECORDS;
					break;
				case 3:
					PostQuitMessage(0);
					return 0;
				}
				odswiez_okno(hOkno);
				return 0;
			case VK_RIGHT:
				if( wybor == 2)
				{
					szybkosc = (szybkosc + 1) % 10;
					odswiez_szybkosc(hOkno);
				}
				return 0;
			case VK_LEFT:
				if( wybor == 2)
				{
					szybkosc--;
					if( szybkosc == -1 )
						szybkosc = 9;
					odswiez_szybkosc(hOkno);
				}
				return 0;
			}
			return 0;
		}
	case BEGIN_GAME:
		switch(Msg)
		{
		case WM_PAINT:
			{
				HDC HDCokno;
				PAINTSTRUCT ps;
				RECT rec;
				GetClientRect(hOkno,&rec);
				HDCokno = BeginPaint(hOkno,&ps);
				DrawTextW(HDCokno, L"Przygotuj si�, nacisnij dowolny klawisz by rozpocz��",53,&rec,
					DT_CENTER | DT_VCENTER);
				EndPaint(hOkno,&ps);
				return 0;
			}
		case WM_KEYDOWN:
			Stan = GAME;
			{
				wynik = 0;
				wynik_char = L"0";
				pozycja_lista = 0;
				imie_lista = std::wstring();
				waz = Snake( 5 , Punkt(225,225) , W, Punkt(450,450), Punkt(5,5), skok);
				wybor = 0;
				odswiez_okno(hOkno);
				SetTimer(hOkno,1,100 - 10*szybkosc,NULL);

			}
			return 0;
		}
		break;
	case GAME:
		switch(Msg)
		{
		case WM_PAINT:
			if( waz.czy_zderzenie() )
			{
				Stan = END_GAME;
				KillTimer(hOkno,1);
				odswiez_okno(hOkno);
				return 0;
			}
			HDC HdcOkno;
			PAINTSTRUCT ps;
			HdcOkno = BeginPaint(hOkno,&ps);
			DeleteObject( SelectObject( HdcOkno, CreatePen(PS_SOLID,0,RGB(0,0,0))) );
			TextOut(HdcOkno,5,460,L"PUNKTY",6);
			TextOut(HdcOkno,100,460, wynik_char.c_str(), wynik_char.length() );
			DeleteObject( SelectObject( HdcOkno, CreateSolidBrush( RGB(255,255,255) )) );
			Rectangle(HdcOkno,5,5,455,455);
			DeleteObject( SelectObject( HdcOkno, CreatePen(PS_SOLID,0,RGB(50,50,126))) );
			for(int i = 5; i < 455; i+=skok)
			{
				for(int j = 5; j < 455; j+=skok)
					Rectangle(HdcOkno,j,i,j+skok,i+skok);
			}
			
			{
				DeleteObject( SelectObject( HdcOkno, CreateSolidBrush( RGB(0,0,0) )) );
				for(std::vector<Punkt>::const_iterator it = waz.beg(); it != waz.end(); it++)
					Rectangle(HdcOkno, (*it).x, (*it).y, (*it).x + skok ,(*it).y + skok );
			}
			DeleteObject( SelectObject( HdcOkno, CreateSolidBrush( RGB(255,0,0) )) );
			Rectangle(HdcOkno, waz.zarcie().x, waz.zarcie().y, waz.zarcie().x + skok , waz.zarcie().y + skok);
			EndPaint(hOkno,&ps);
			return 0;
		case WM_TIMER:
			{
				int xx,yy;
				RECT b,e;
				e.left = xx =(*(waz.end()-1)).x;
				e.top = yy =(*(waz.end()-1)).y;
				e.right = xx + skok;
				e.bottom = yy + skok;
				waz.ruch();
				b.left = xx = (*waz.beg()).x;
				b.top = yy = (*waz.beg()).y;
				b.right = xx + skok;
				b.bottom = yy + skok;
				if( waz.czy_przyrost() )
				{
					wynik++;
					{
						std::wostringstream out;
						out << wynik;
						wynik_char = out.str();
					}
					RECT z,w;
					z.left = waz.zarcie().x;
					z.top = waz.zarcie().y;
					z.right = waz.zarcie().x + skok;
					z.bottom = waz.zarcie().y + skok;
					w.left = 100;
					w.top = 460;
					w.right = 160;
					w.bottom = 480;
					InvalidateRect(hOkno,&z,true);
					InvalidateRect(hOkno,&w,true);
				}
				InvalidateRect(hOkno,&b,true);
				InvalidateRect(hOkno,&e,true);
				poruszony = false;
			}
			return 0;
		case WM_KEYDOWN:
			switch(wParam)
			{
			case VK_ESCAPE:
				PostQuitMessage(0);
				return 0;
			case VK_LEFT:
				if( ! poruszony )
					waz.zmien_kierunek(W);
				poruszony = true;
				return 0;
			case VK_RIGHT:
				if( ! poruszony )
					waz.zmien_kierunek(E);
				poruszony = true;
				return 0;
			case VK_UP:
				if( ! poruszony )
					waz.zmien_kierunek(N);
				poruszony = true;
				return 0;
			case VK_DOWN:
				if( ! poruszony )
					waz.zmien_kierunek(S);
				poruszony = true;
				return 0;
			}
			return 0;
		}
		break;
		case END_GAME:
			switch(Msg)
			{
			case WM_PAINT:
				{
					HDC HDCokno;
					PAINTSTRUCT ps;
					RECT rec;
					GetClientRect(hOkno,&rec);
					HDCokno = BeginPaint(hOkno,&ps);
					DrawTextW(HDCokno, L"Koniec gry, nacisnij dowolny klawisz by zako�czy�",50,&rec,
						DT_CENTER | DT_VCENTER);
					EndPaint(hOkno,&ps);
					return 0;
				}
			case WM_KEYDOWN:
				{
					Player gracze[10];
					std::wfstream in(plik);
					if( ! in.is_open() )
					{
						tworz_domyslny_plik();
						in.close();
						in.open(plik);
					}
					for(int i = 0; i < 10; i++)
					{
						in >> gracze[i].imie >> gracze[i].punkty;
						if( wynik < gracze[i].punkty )
							pozycja_lista++;
					}
					if( pozycja_lista < 10 )
						Stan = BEAT_RECORD;
					else
						Stan = RECORDS;
	
					odswiez_okno(hOkno);
				}
				return 0;
			}
		case BEAT_RECORD:
			switch(Msg)
			{
			case WM_PAINT:
				{
					HDC HDCokno;
					PAINTSTRUCT ps;
					RECT rec;
					GetClientRect(hOkno,&rec);
					HDCokno = BeginPaint(hOkno,&ps);
					SetBkMode(HDCokno,TRANSPARENT);
					DrawTextW(HDCokno, L"Gratulacje pobi�es rekord, podaj swoje imie i wcisnij ENTER",70,&rec,
						DT_CENTER | DT_WORDBREAK);
					TextOut(HDCokno,140,150,L"->",2);
					TextOut(HDCokno,150,150,imie_lista.c_str(),imie_lista.length());
					EndPaint(hOkno,&ps);
					return 0;
				}
			case WM_KEYDOWN:
				if( wParam == VK_RETURN )
				{
					Stan = RECORDS;
					zapisz_do_pliku(imie_lista, wynik, pozycja_lista);
					odswiez_okno(hOkno);
				}
				return 0;
			case WM_CHAR:
				if( wParam == VK_BACK )
				{
					if( imie_lista.length() > 0)
						imie_lista.erase( imie_lista.end() - 1 );
				}
				else if( imie_lista.length() < 5 && isalnum( char(wParam) ) )
					imie_lista +=  char(wParam);
				{
					RECT r;
					r.left = 150;
					r.top = 150;
					r.right = 300;
					r.bottom = 200;
					InvalidateRect(hOkno,&r,true);
				}
				return 0;
			}
		case RECORDS:
			switch(Msg)
			{
			case WM_PAINT:
				{
					HDC HDCokno;
					PAINTSTRUCT ps;
					RECT rec;
					GetClientRect(hOkno,&rec);
					std::wifstream in(plik);
					if( ! in.is_open() )
					{
						tworz_domyslny_plik();
						in.close();
						in.open(plik);
					}
					HDCokno = BeginPaint(hOkno,&ps);
					SetBkMode(HDCokno,TRANSPARENT);
					DeleteObject( SelectObject( HDCokno,CreateFontW(12,12,0,0,800,0,0,0,0,0,0,0,0,L"Arial")) );
					TextOut(HDCokno,155,5, L"REKORDY",7);
					for(int i = 0; i < 10; i++)
					{
						int wynik;
						std::wstring imie;
						std::wostringstream out, out2;
						in >> imie;
						in >> wynik;
						out << (i+1) << ".";
						TextOut(HDCokno, 5,50 + i*40, out.str().c_str(), out.str().length());
						TextOut(HDCokno, 55,50 + i*40, imie.c_str(), imie.length());
						out2 << wynik;
						TextOut(HDCokno, 255,50 + i*40, out2.str().c_str(), out2.str().length());
					}
					TextOut(HDCokno,0,450,L"Kliknij Esc by wrocic do menu",30);
					EndPaint(hOkno,&ps);
					in.close();
					return 0;
				}
			case WM_KEYDOWN:
				if( wParam == VK_ESCAPE )
				{
					Stan = MENU;
					odswiez_okno(hOkno);
					return 0;
				}
			}
		}
		return DefWindowProc(hOkno,Msg,wParam,lParam);
}
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE, LPSTR, int CmdShow )
{
	srand( unsigned(time(0)) );
	WNDCLASSEX WindowClass;
	ZeroMemory( &WindowClass, sizeof(WNDCLASSEX) );
	WindowClass.cbSize = sizeof(WNDCLASSEX);
	WindowClass.hbrBackground = CreateSolidBrush( RGB(0,255,0) );
	WindowClass.hCursor = NULL;
	WindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WindowClass.hInstance = hInstance;
	WindowClass.lpfnWndProc = WindowEventProc;
	WindowClass.lpszClassName = WindowClassName.c_str();

	RegisterClassEx(&WindowClass);

	HWND hOkno = CreateWindowExW(WS_EX_TOPMOST | WS_EX_APPWINDOW | WS_EX_CLIENTEDGE , WindowClassName.c_str(),
		L"Snejk", WS_VISIBLE, 50, 50, 475, 515, NULL, NULL, hInstance, NULL);

	ShowWindow(hOkno, CmdShow);
	UpdateWindow(hOkno);


	MSG Msg;
	while( GetMessage( &Msg, NULL, 0, 0 ) )
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	KillTimer(hOkno,1);
	return static_cast<int>(Msg.wParam);
}