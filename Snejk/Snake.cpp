#include<vector>
#include<cassert>
#include<algorithm>
#include"Snake.h"

using namespace std;

bool Snake:: ruch()
{
	Punkt czubek;
	switch(zwrot)
	{
	case N:
		czubek.x = punkty[0].x;
		czubek.y = punkty[0].y - skok;
		break;
	case S:
		czubek.x = punkty[0].x;
		czubek.y = punkty[0].y + skok;
		break;
	case W:
		czubek.x = punkty[0].x - skok;
		czubek.y = punkty[0].y;
		break;
	case E:
		czubek.x = punkty[0].x + skok;
		czubek.y = punkty[0].y;
		break;
	}

	Punkt dodatkowy = punkty[punkty.size() - 1]; 
	for(unsigned int i = punkty.size() - 1; i > 0; i--)
		punkty[i] = punkty[i-1];
	punkty[0] = czubek;

	rosnie = false;
	if( czubek == jedzenie )
	{
		rosnie = true;
		jedzenie = losuj_jedzenie();
	}

	if( rosnie )
	{
		punkty.push_back(dodatkowy);
		++dlugosc;
	}
	return true;
}

bool Snake::czy_zderzenie()
{
	return punkty[0].x < granica_dolna.x || punkty[0].y < granica_dolna.y ||
		punkty[0].x > granica_gorna.x || punkty[0].y > granica_gorna.y || 
		std::find(punkty.begin()+1, punkty.end(), *punkty.begin() ) != punkty.end();
}

bool Snake::zmien_kierunek(Kierunek k)
{
	if( (zwrot == N && k == S) || (zwrot == S && k == N) || (zwrot == E && k ==W) ||
		(zwrot == W && k == E) || (zwrot == k) )
		return false;
	else
		zwrot = k;
	return true ;
}

Punkt Snake::losuj_jedzenie()
{
	Punkt temp;
	while(1)
	{
		temp.x = 5 + 10 * ( rand() % 45 );
		temp.y = 5 + 10 * ( rand() % 45 );
		if( std::find( punkty.begin(), punkty.end(), temp ) == punkty.end() )
			return temp;
	}
}